var mongoose = require('mongoose');
var schema = mongoose.Schema;
var userSchema = new schema({
	name : {
		type:String
	},
	email : {
		type:String
	},
	created_date:{
		type:Date,
		default: Date.now()
	},
	contact:  {
		type: Number
	},
	imageName: {
		type:String
	},
	imagePath:{
		type : String
	},
	address:{
		type:String
	},
	skills:[]
})
module.exports = mongoose.model('Users',userSchema);