var Users = require('./users.model');
var Promise = require('promise');
module.exports = {
	getUserById: function(userObject){
		return new Promise(function (resolve, reject) {

			Users.findOne(userObject,function (err,response) {
				if(err){
					reject(err);
				}
				else{
					resolve(response);
				}
			});
		})
	},
	saveUser : function(userObject){
		return new Promise(function (resolve, reject) {
			var user = new Users(userObject);
			user.save(function (err,response) {
				if(err){
					reject(err);
				}
				else{
					resolve(response);
				}
			})
		})
		
		 
	},
	getUsers : function(userObject){
		return new Promise(function (resolve, reject) {
			Users.find(function (err,response) {
				if(err){
					reject(err);
				}
				else{
					resolve(response);
				}
			})
		})
		
	},
	updateUser: function(findObject,userObject){
		if(userObject._id){
			delete userObject['_id'];
		}
		return new Promise(function (resolve, reject) {
			Users.update(findObject, {$set: userObject},function (err,response) {
				if(err){
					reject(err);
				}
				else{
					resolve(response)
				}
			})
		})
	},
	deleteUser : function (findObject) {
		return new Promise(function (resolve, reject) {
			Users.remove(findObject,function (err,response) {
				if(err){
					reject(err);
				}
				else{
					resolve(response);
				}
			})
		})
	}
} 