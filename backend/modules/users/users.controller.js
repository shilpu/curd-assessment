var Users = require('./users.model');
var apiCodes = require('../../config/apicodes');
var messages = require('../../config/messages');
var userService = require('./users.service');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;


module.exports = function(app){
	app.saveUsers  = function(req,res){
		req.body = req.body.payload ? JSON.parse(req.body.payload) : req.body;
		userService.getUserById({email:req.body.email}).then((response)=>{
			if(response){
				return res.status(apiCodes.DUPLICATE).send({message:messages.USER_ALREADY_EXIST});
			}
			else{	
					if(req.file && req.file.originalname){
						req.body.imagePath = req.protocol + '://'+ req.headers.host + '/' + 'images/' + req.file.filename;
						req.body.imageName = req.file.filename;
					}
					
					userService.saveUser(req.body).then((response)=>{
					if(response){
						return res.status(apiCodes.SUCCESS).send({message:messages.USER_SAVED});
						
					}
					}).catch((err)=>{
						return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({message:messages.ERROR_WHILE_SAVING});
					})
				
			}
		}).catch((err)=>{
					
					return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({message:messages.ERROR_WHILE_FINDING});
				})
	
	}
	app.getUsers = function(req,res){
		userService.getUsers().then((response)=>{
			return res.jsonp(response);
		}).catch((error)=>{
			console.log("ERROR :: ",error)
			return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({message:messages.ERROR_WHILE_FINDING});
		})
	
	}
	app.getUserById = function(req,res){
		userService.getUserById({_id:ObjectId(req.params.id)}).then((response)=>{
			return res.jsonp(response);
		}).catch((error)=>{
			return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({message:messages.ERROR_WHILE_FINDING});
		})
	
	}
	app.updateUser = function(req,res){
		req.body = req.body.payload ? JSON.parse(req.body.payload) : req.body;
		if(req.file && req.file.originalname){
			req.body.imagePath = req.protocol + '://'+ req.headers.host + '/' + 'images/' + req.file.filename;
			req.body.imageName = req.file.filename;
		}
		userService.updateUser({_id:ObjectId(req.params.id)},req.body).then((response)=>{
			return res.jsonp(response);
		}).catch((error)=>{
			return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({message:messages.ERROR_WHILE_UPDATING});
		})
	}
	app.deleteUser = function (req,res) {
		userService.deleteUser({_id:ObjectId(req.params.id)}).then((response)=>{
			return res.jsonp(response);
		}).catch((error)=>{
			return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({message:messages.ERROR_WHILE_DELETING});
		})
	}
}