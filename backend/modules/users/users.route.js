var multer  = require('multer')
var upload = multer({ dest: __dirname + '/../../public/images/' });
console.log("__dirname",__dirname);
module.exports = function(app){
	app.route('/api/v1/users').post(upload.single('file'),app.saveUsers);
	app.route('/api/v1/users').get(app.getUsers);
	app.route('/api/v1/users/:id').get(app.getUserById)
	.put(upload.single('file'),app.updateUser).delete(app.deleteUser);
}