var express = require('express'),
fs = require('fs'),
bodyParser = require('body-parser'),
mongoose = require('mongoose'),
dbConfig = require('./config/databases'),
cors = require('cors');
var app = new express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static('public'))

app.use(cors());

mongoose.connect(dbConfig.dbSettings.url);

mongoose.connection.on("connected",function(){
    console.log('Mongoose default connection open to ' + dbConfig.dbSettings.url);
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

//load all controllers
 var moduleFiles = fs.readdirSync(__dirname + '/modules');
// load all models
for (var i = 0; i < moduleFiles.length; i++) {
    loadModules(moduleFiles[i], "model");
}
//load all controllers
for (var j = 0; j < moduleFiles.length; j++) {
    loadModules(moduleFiles[j], "controller");
}

// load routes
for (var j = 0; j < moduleFiles.length; j++) {
    loadModules(moduleFiles[j], "route");
}

function loadModules(module,type){
	var moduleLocation = __dirname + '/modules/' + module;
	var moduleFiles = fs.readdirSync(__dirname + '/modules/' + module);
	moduleFiles.map((file)=>{
		 var fileName = file.split('.');
		  if (fileName && fileName.length > 0) {
		  	  var fileType = fileName[1];
		  	   if (fileType === type && type ==='model') {
                // it is a model.
                return require(moduleLocation + '/' + file);
            }
            else if (fileType === type) {
                // it is a controller
                return (require(moduleLocation + '/' + file))(app);
            }
		  }
	})

}
// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
});

app.listen(3000,()=>{
	console.log('--------server connected on 3000');
})
module.exports = app;