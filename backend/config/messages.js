module.exports = {
	USER_SAVED : 'User successfully saved',
	ERROR_WHILE_SAVING : 'Error while saving',
	ERROR_WHILE_FINDING : 'Error while finding',
	USER_NOT_FOUND : 'User not found',
	USER_ALREADY_EXIST : 'User Already Exist',
	ERROR_WHILE_UPDATING : 'Error while updating',
	ERROR_WHILE_DELETING : 'Error while deleting'
}