import {Directive, ElementRef, HostListener, Input, OnChanges} from '@angular/core';

@Directive({
    selector: '[numbersOnly]'
})
export class NumbersOnlyDirective implements OnChanges {

    @Input() numbersOnly: any;

    constructor(private el: ElementRef) {}

    @HostListener('keydown', ['$event'])
    keyDownEvent(event: KeyboardEvent) {
        let keyCondition = (event.which < 48 || event.which > 57);
        if(event.which>95 && event.which<106){
            keyCondition = (event.which < 96 || event.which > 105);
        }
        // Add other conditions if need to allow ctr+c || ctr+v
        if (event.key.length === 1 && keyCondition) {
            event.preventDefault();
        }
       
    }

    ngOnChanges(changes) {
       /* if (changes.numbersOnly) {
            this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^0-9]/g, '');
        }*/
    }

}