import {  Routes }  from '@angular/router';
import {UserRoute} from './user/user.routing';
export const appRoutes: Routes = [
 	{ path: '',   redirectTo: '/users', pathMatch: 'full' },
 	...UserRoute
];