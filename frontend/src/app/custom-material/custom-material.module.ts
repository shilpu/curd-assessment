import { NgModule } from '@angular/core';
import {MatToolbarModule, MatInputModule, MatButtonModule, 
MatCardModule,MatTooltipModule,MatTableModule,MatFormFieldModule,MatIconModule,MatSnackBarModule} from '@angular/material';
@NgModule({
  imports: [MatToolbarModule, MatInputModule, MatButtonModule, MatCardModule,MatTooltipModule,MatTableModule,MatFormFieldModule,MatIconModule,MatSnackBarModule],
  exports: [MatToolbarModule, MatInputModule, MatButtonModule, MatCardModule,MatTooltipModule,MatTableModule,MatFormFieldModule,MatIconModule,MatSnackBarModule	]
})
export class CustomMaterialModule { }