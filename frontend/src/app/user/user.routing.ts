import { UserListComponent } from './user-list/user-list.component';
import { UserAddComponent } from './user-add/user-add.component';
import { Routes } from '@angular/router';

export const UserRoute : Routes = [
 { path: 'users', component: UserListComponent },
 { path: 'users/:id', component: UserAddComponent },
 { path: 'add', component: UserAddComponent },
 { path: '',   component: UserListComponent  },
]

