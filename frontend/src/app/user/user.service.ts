import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {AppConfig} from '../app.config';
@Injectable()


export class UserService {
	constructor(private http:Http){}
	saveUser(data){
	 	return this.http.post(AppConfig.API_URL + 'users',data).map((res:Response) => res.json());
	}
	editUser(data,userId){
	 	return this.http.put(AppConfig.API_URL + 'users/'+userId,data).map((res:Response) => res.json());
	}
	getUsers(){
	 	return this.http.get(AppConfig.API_URL + 'users').map((res:Response) => res.json());
	   
	}
	getUserById(userId){
	 	return this.http.get(AppConfig.API_URL + 'users/'+userId).map((res:Response) => res.json());
	   
	}
	deleteUser(userId){
		return this.http.delete(AppConfig.API_URL + 'users/'+userId).map((res:Response) => res.json());
	}
}
