import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  dataSource : any = [];
  constructor(private userService : UserService,public snackBar: MatSnackBar){}

	displayedColumns = ['position', 'name', 'email', 'contact','edit','delete'];

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this.userService.getUsers().subscribe((response)=>{
      console.log("Response : ",response)
      this.dataSource = response;
      this.dataSource.map((data,index)=>{
        data.position = index;
      })
    },(error)=>{

    })
  }
  deleteUser(userId,index){

    this.userService.deleteUser(userId).subscribe((response)=>{
      this.getUsers()
    
       this.snackBar.open('User successfully deleted', '', {
        duration: 2000,
      });
    },(error)=>{
      console.log("error",error)
    })
  }


  

}

