import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
	user : any = {};
	file : any ;
  constructor(private userService : UserService,private router:Router,private route : ActivatedRoute) { }

  ngOnInit() {
  	let userId = this.route.snapshot && this.route.snapshot.params ? this.route.snapshot.params['id'] : null;
  	if(userId && userId != 'add'){
      this.getUserById(userId);
  	}
  	
  }
  uploadFile(event){
  	this.file   = event.target && event.target.files ? event.target.files[0] : null;
    if(this.file){
      this.user.imagePath = null
    }
    
  }
  saveUser(){
  	let formData =  new FormData();
  	if(this.file){
  		formData.append('file',this.file);
  	}
  	formData.append('payload',JSON.stringify(this.user));
  	this.userService.saveUser(formData).subscribe((response)=>{
  		this.router.navigate(['users']);
  	},(error)=>{

  	})
  }
  editUser(){
  	let formData =  new FormData();
  	if(this.file){
  		formData.append('file',this.file);
  	}
  	formData.append('payload',JSON.stringify(this.user));
  	this.userService.editUser(formData,this.user._id).subscribe((response)=>{
  		this.router.navigate(['users']);
  	},(error)=>{

  	})
  }

  getUserById(userId){
  	this.userService.getUserById(userId).subscribe((response)=>{
      this.user = response;
  	},(error)=>{

  	})
  }

}
