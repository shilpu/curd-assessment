import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { UserListComponent } from './user-list/user-list.component';
import { UserAddComponent } from './user-add/user-add.component';
import { FormsModule } from '@angular/forms';
import {CustomMaterialModule}  from '../custom-material/custom-material.module';
import {CommonModule} from '@angular/common';
import {validationModule} from '../validations/validation.module';
import {UserService} from './user.service';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import { TagInputModule } from 'ngx-chips';



@NgModule({
  declarations: [
  UserListComponent,
  UserAddComponent],
  imports: [
 	CustomMaterialModule,
 	FormsModule,
 	CommonModule,
 	BrowserAnimationsModule,
 	validationModule,
 	HttpModule,
 	RouterModule,
 	TagInputModule
  ],
  providers: [UserService]
})
export class UserModule { }
