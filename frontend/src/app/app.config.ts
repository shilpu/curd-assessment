import {environment} from '../environments/environment';
export class AppConfig{
	public static API_URL = environment.API_URL;
}